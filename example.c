/* Author: Aimar Ibarra <greenerclay@gmail.com>
 *
 * This file shows a simple example of a test trough gcut.
 *
 * To build and run the file:
 * ```sh
 * git clone https://gitlab.com/AimarIbarra/gcut.git
 * cd gcut
 * cc example.c && ./a.out
 * ```
 */
#include "gcut.h"

SUITE(Arith, {
    /* If any test is defined, the setup and teardown procedures
     * also need to be defined, but can be empty.
     *
     * Where this macros are invoked is irrelecant, since gcut
     * uses goto statements to acces them.
     */
    SETUP();
    TEARDOWN();
    
    TEST(one_plus_one_is_two, {
        ASSERT(1 + 1 == 2);
      });
    TEST(two_times_two_is_six, {
        ASSERT(2 * 2 == 6); // Will fail
      });
    ASSERT(1 == 2); // The assertion will fail but the suite won't
  });

SUITE(Pass, {}); // The empty suite will pass by default
