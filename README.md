# GCUT
Simple unit testing macros for gnu compilers.

## Install

gcut is and allways will be a header only project, so just get a copy of the header files and include them in your tests.

Lets make a dummy project:
```shell
mkdir gcutdummy
cd gcutdummy
```
Now lets include gcut as a submodule:
```shell
git submodule add git@gitlab.com:Aimar_Ibarra/gcut.git
git submodule init
git submodule update
```

Lets make a simple `test.c` file:
```c
// ~/gcutdummy/test.c
#include "gcut/gcut.h"

SUITE(MyTest, {
    SETUP();
    TEARDOWN();
    TEST(two_plus_two_is_four, {
        ASSERT(2 + 2 == 4);
        });
    });
```

And finally lets compile and run it:
```shell
cc test.c && ./a.out
```

## DOCS
To know how to use all the features of gcut please refer to the wiki.
