# CONTRIBUTING

Before making any contribution open a new issue.

Get a fork of this project and create a pull request with your changes.

Link the issue in the PR, and it will then be reviewed.

Additionally if you want you may add your name and email to a contributors file, if there is any, or create it yourself.

The person who accepted your change will be responsible to update the changelog file.
