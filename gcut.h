#ifndef GCUT_H
#define GCUT_H

#include <stdio.h>

#ifndef GCUT_NO_COLOR
#define ___GCUT_RED "\033[1;31m"
#define ___GCUT_GREEN "\033[0;32m"
#define ___GCUT_YELLOW "\033[0;33m"
#define ___GCUT_RESET "\033[0m"
#else
#define ___GCUT_RED ""
#define ___GCUT_GREEN ""
#define ___GCUT_YELLOW ""
#define ___GCUT_RESET ""
#endif

extern int gcut_argc;
extern char **gcut_argv;

#ifdef GCUT_NO_MAIN
#define GCUT_EXEC                                                              \
  int gcut_argc;                                                               \
  char **gcut_argv;                                                            \
  int main(int c, char **a) {                                                  \
    gcut_argc = c;                                                             \
    gcut_argv = a;                                                             \
    return 0;                                                                  \
  }
#else
int gcut_argc;
char **gcut_argv;
int main(int c, char **a) {
  gcut_argc = c;
  gcut_argv = a;
}
#endif

#ifdef GCUT_NO_PASS
#define ___GCUT_LOG_PASS()
#define ___GCUT_LOG_IF_FAIL if (___gcut_failed)
#else
#define ___GCUT_LOG_PASS()                                                     \
  fprintf(stderr,                                                              \
          ___GCUT_GREEN "[ PASSED ] "___GCUT_RESET                             \
                        "%s:%s\n",                                             \
          ___gcut_suite_name, ___gcut_test_name)
#define ___GCUT_LOG_IF_FAIL
#endif

#define ASSERT(cond)                                                           \
  do {                                                                         \
    if (cond)                                                                  \
      break;                                                                   \
    if (___gcut_test_name) {                                                   \
      fprintf(stderr,                                                          \
              ___GCUT_RED "[ FAILED ]" ___GCUT_RESET " %s:%s " ___GCUT_YELLOW  \
                          "%s\n" ___GCUT_RESET,                                \
              ___gcut_suite_name, ___gcut_test_name, #cond);                   \
      ___gcut_failed = 1;                                                      \
      goto ___gcut_teardown;                                                   \
    } else {                                                                   \
      fprintf(stderr,                                                          \
              ___GCUT_RED "Failed assertion in %s: "___GCUT_YELLOW             \
                          "%s\n" ___GCUT_RESET,                                \
              ___gcut_suite_name, #cond);                                      \
    }                                                                          \
  } while (0)

#define SUITE(name, ...)                                                       \
  __attribute__((destructor)) static void ___GCUT_##name##_SUITE(void) {       \
    int ___gcut_tests = 0;                                                     \
    int ___gcut_failed = 0;                                                    \
    const char ___gcut_suite_name[] = #name;                                   \
    const char *___gcut_test_name = NULL;                                      \
    void *___gcut_next_label;                                                  \
    __VA_ARGS__                                                                \
    ___GCUT_LOG_IF_FAIL                                                        \
    fprintf(stderr,                                                            \
            "%s[ %s ]"___GCUT_RESET                                            \
            " Ran %i tests in " #name "\n",                                    \
            ___gcut_failed ? ___GCUT_RED : ___GCUT_GREEN,                      \
            ___gcut_failed ? "FAILED" : "PASSED", ___gcut_tests);              \
  }                                                                            \
  static void ___GCUT_##name##_SUITE(void)

#define TEST(name, ...)                                                        \
  do {                                                                         \
    ++___gcut_tests;                                                           \
    ___gcut_test_name = #name;                                                 \
    ___gcut_next_label = &&name;                                               \
    goto ___gcut_setup;                                                        \
  name:                                                                        \
    ___gcut_next_label = &&___gcut_teardown_##name;                            \
    __VA_ARGS__                                                                \
    ___GCUT_LOG_PASS();                                                        \
    goto ___gcut_teardown;                                                     \
    ___gcut_teardown_##name : ___gcut_test_name = NULL;                        \
  } while (0)

#define SETUP(...)                                                             \
  do {                                                                         \
    if (0) {                                                                   \
    ___gcut_setup:                                                             \
      __VA_ARGS__                                                              \
      goto *___gcut_next_label;                                                \
    }                                                                          \
  } while (0)

#define TEARDOWN(...)                                                          \
  do {                                                                         \
    if (0) {                                                                   \
    ___gcut_teardown:                                                          \
      __VA_ARGS__                                                              \
      goto *___gcut_next_label;                                                \
    }                                                                          \
  } while (0)

#endif
